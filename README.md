# cover-generator

图片生成组件

# 类型及参数定义

### DataSource

| 参数名              | 类型                                                 | 说明                                                                               |
| ------------------- | ---------------------------------------------------- | ---------------------------------------------------------------------------------- |
| type                | "rect"\| "image" \| "qrcode" \| "text" \| "richtext" | "rect"矩形，"image"图片，"qrcode"二维码，"text"文本，"richtext"组合文本            |
| image               | String\| HTMLImageElement \| HTMLCanvasElement       | 类型为"image"时，填入图片链接或dataURI或图像元素；类型为"qrcode"时，填入二维码链接 |
| text                | String\| DataSource[]                                | 类型为"text"时，填入文本内容；类型为"richtext"时，填入子数据数组，类型与此同       |
| x、y、width、height | Number                                               | 元素的位置和宽高                                                                   |
| r                   | Number\| Number[]                                    | 圆角属性，可为1、[1, 1, 1, 1]、[1, 2]、[1, 2, 3]形式                               |
| colorDark           | String                                               | 二维码背景色                                                                       |
| colorLight          | String                                               | 二维码颜色                                                                         |
| correctLevel        | "L"\| "M" \| "Q" \| "H"                              | 二维码纠错等级                                                                     |
| 其他属性            | 见zrender文档中Displayable元素的style属性            | 其他属性会透传给zrender的style                                                     |



### new CoverGenerator(dom, opts)

| 参数名                  | 类型                  | 说明                                                         |
| ----------------------- | --------------------- | ------------------------------------------------------------ |
| dom                     | HTMLDivElement\| Null | 初始化位置，可不填                                           |
| opts.width、opts.height | Number\| String       | 生成的图片的宽高                                             |
| opts.dataSource         | DataSource[]          | 数据源数组                                                   |
| opts.imageType          | String                | 生成的图片类型，设置为"jpg"即为"image/jpeg"，其他值均为"image/png" |
| opts.imageQuality       | Number                | 0~1之间的值，默认为0.5，当opts.imageType为"jpg"时生效，详见HTMLCanvasElement.toDataURL的参数介绍 |



### CoverGeneratorInstance.setDataSource(dataSource)

| 参数名     | 类型         | 说明           |
| ---------- | ------------ | -------------- |
| dataSource | DataSource[] | 重新设置数据源 |



### CoverGeneratorInstance.generate(cb)

| 参数名 | 类型     | 说明                                                |
| ------ | -------- | --------------------------------------------------- |
| cb     | Function | 当图片生成完成后，回调cb并传入图片的dataURI作为参数 |



# 示例

```javascript
import CoverGenerator from "CoverGenerator";

const gen = new CoverGenerator(null, {
  width: 400,
  height: 400,
  dataSource: [{
    type: "rect",
    x: 0,
    y: 0,
    width: 400,
    height: 400,
    r: 36,
    fill: "#8ae0ec"
  }, {
    type: "rect",
    x: 168,
    y: 32,
    width: 64,
    height: 64,
    r: 64,
    fill: "#fff"
  }, {
    type: "image",
    image: "https://cloudflare.luhawxem.com/img/Avatar.jpg",
    x: 170,
    y: 34,
    width: 60,
    height: 60,
    r: 60
  }, {
    type: "rect",
    x: 340,
    y: 340,
    width: 40,
    height: 40,
    r: 4,
    fill: "#fff"
  }, {
    type: "qrcode",
    image: "https://cloudflare.luhawxem.com",
    x: 342,
    y: 342,
    width: 36,
    height: 36,
    colorDark: "#fff", // 背景色
    colorLight: "#8ae0ec", // 二维码颜色
    correctLevel: "L" // 纠错等级 L/M/Q/H
  }, {
    type: "text",
    text: "Hello world",
    x: 36,
    y: 340,
    fill: "red",
    fontSize: 24,
  }, {
    type: "richtext",
    text: [{
      type: "text",
      text: "亻尔",
      fill: "pink",
      fontSize: 36,
      textPadding: 14,
      textBackgroundColor: "yellow",
    }, {
      type: "text",
      text: "女子",
      fill: "purple",
      fontSize: 48,
    }],
    x: 64,
    y: 128,
    fill: "blue", // 如果各自设定了fill, 这里就没用了
  }],
  imageType: "jpg",
  imageQuality: 0.7
})
gen.generate((data) => {
  const img = document.createElement("img");
  img.style = "width: 400px; height: 400px;";
  img.src = data;
  document.body.appendChild(img);
})
```
