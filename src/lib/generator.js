import "../external/zrender.js";
import "../external/qrcode.js";
import { isDataSourceValid, observeDOM, preloadImage, typeOf } from "./utils.js";

// https://ecomfe.github.io/zrender-doc/public/api.html
const zrender = window.zrender;
const QRCode = window.QRCode;

class Generator {
  #loaded = false;
  #devicePixelRatio = 1;
  #dom;
  #opts;
  #zr;
  /**
   * @param {HTMLDivElement} dom 
   * @param {{
   *   width: Number | String,
   *   height: Number | String,
   *   dataSource: {
   *     type: "rect" | "image" | "qrcode" | "text" | "richtext",
   *     image: String | HTMLImageElement | HTMLCanvasElement,
   *     text: String | dataSource[],
   *     x: Number,
   *     y: Number,
   *     width: Number,
   *     height: Number,
   *     r: Number | Number[],
   *     fill: String,
   *     fontWeight: String,
   *     fontSize: Number,
   *     fontFamily: String,
   *     textLineHeight: Number,
   *     textBackgroundColor: String,
   *     textPadding: Number | Number[],
   *     textAlign: String,
   *     textVerticalAlign: String,
   *     colorDark: String,
   *     colorLight: String,
   *     correctLevel: String
   *   }[],
   *   imageType: String,
   *   imageQuality: Number
   * }} opts
   */
  constructor(dom, opts) {
    if (!dom || dom.nodeName !== "DIV") {
      dom = document.createElement("div");
    }
    dom.style.width = opts.width * this.#devicePixelRatio;
    dom.style.height = opts.height * this.#devicePixelRatio;
    this.#dom = dom;
    opts.imageType = (opts.imageType && opts.imageType.toLowerCase() === "jpg") ? "image/jpeg" : "image/png";
    opts.imageQuality = opts.imageQuality ? opts.imageQuality : 0.5;
    this.#opts = opts;
    // https://ecomfe.github.io/zrender-doc/public/api.html#zrenderinitdom-opts 此处width和height用auto并不行,原因不明
    this.#zr = zrender.init(dom, {
      devicePixelRatio: this.#devicePixelRatio,
      width: this.#opts.width * this.#devicePixelRatio,
      height: this.#opts.height * this.#devicePixelRatio,
    })
    if (!isDataSourceValid(opts.dataSource)) {
      console.log("数据为空，请在后续过程中调用setDataSource设置数据");
      return;
    }
    this.#processDataSource();
  }

  /**
   * @description 预处理dataSource, 生成qrcode以及预加载图片
   */
  #processDataSource() {
    const dataSource = this.#opts.dataSource;
    let preload = 0;
    const waitingProcess = () => {
      if (preload !== 0) {
        setTimeout(() => waitingProcess(), 300);
        return;
      }
      this.#loaded = true;
    }
    dataSource.forEach((item) => {
      switch(item.type) {
        case "qrcode":
          const qrcodeDiv = document.createElement("div");
          const qrcode = new QRCode(qrcodeDiv, {
            width: 500,
            height: 500,
            colorDark: item.colorDark || "#000",
            colorLight: item.colorLight || "#fff",
            correctLevel: item.correctLevel ? QRCode.CorrectLevel[item.correctLevel] : QRCode.CorrectLevel.H
          })
          qrcode.makeCode(item.image);
          item.image = qrcodeDiv.childNodes[0];
          return;
        case "image":
          if (typeOf(item.image) !== "string" || !/^http(s?):\/\//.test(item.image)) {
            return;
          }
          preload++;
          preloadImage(item.image).then((img) => {
            preload--;
            item.image = img;
          });
          return;
        case "richtext":
          let text = "";
          const rich = {};
          const datas = item.text;
          for (let i = 0; i < datas.length; i++) {
            text += `{rich${i}|${datas[i].text}}`;
            rich[`rich${i}`] = datas[i];
            // 富文本样式与zrender稍有不同
            if (datas[i].fill) {
              rich[`rich${i}`].color = datas[i].fill;
            }
            if (datas[i].textBackgroundColor) {
              rich[`rich${i}`].backgroundColor = datas[i].textBackgroundColor;
            }
            if (datas[i].textPadding) {
              rich[`rich${i}`].padding = datas[i].textPadding;
            }
          }
          item.text = text;
          item.rich = rich;
          return;
        default:
          return;
      }
    })
    this.#opts.dataSource = dataSource;
    waitingProcess();
  }

  /**
   * @param {{
  *   image: String | HTMLImageElement | HTMLCanvasElement,
  *   text: String | dataSource[],
  *   x: Number,
  *   y: Number,
  *   width: Number,
  *   height: Number,
  *   r: Number | Number[],
  *   fill: String,
  *   fontWeight: String,
  *   fontSize: Number,
  *   fontFamily: String,
  *   textLineHeight: Number,
  *   textBackgroundColor: String,
  *   textPadding: Number | Number[],
  *   textAlign: String,
  *   textVerticalAlign: String,
  *   colorDark: String,
  *   colorLight: String,
  *   correctLevel: String
  * }} opts
  */
  #addRect(opts) {
    // 去除额外参数image/text/colorDark/colorLight/correctLevel
    const { image, text, colorDark, colorLight, correctLevel, r, x, y, width, height,  ...style } = opts 
    const rect = new zrender.Rect({
      style,
      shape: { r, x, y, width, height }
    })
    this.#zr.add(rect);
  }

  /**
   * @param {{
  *   image: String | HTMLImageElement | HTMLCanvasElement,
  *   text: String | dataSource[],
  *   x: Number,
  *   y: Number,
  *   width: Number,
  *   height: Number,
  *   r: Number | Number[],
  *   fill: String,
  *   fontWeight: String,
  *   fontSize: Number,
  *   fontFamily: String,
  *   textLineHeight: Number,
  *   textBackgroundColor: String,
  *   textPadding: Number | Number[],
  *   textAlign: String,
  *   textVerticalAlign: String,
  *   colorDark: String,
  *   colorLight: String,
  *   correctLevel: String
  * }} opts
  */
  #addImage(opts) {
    // 去除额外参数text/colorDark/colorLight/correctLevel
    const { text, colorDark, colorLight, correctLevel, r, ...style } = opts;
    const image = new zrender.Image({
      style
    });
    // Image本身不带圆角属性, 需要进行裁切
    if (r && (Array.isArray(r) || r !== 0)) {
      const clip = new zrender.Rect({
        shape: {
          r,
          x: style.x,
          y: style.y,
          width: style.width,
          height: style.height,
        }
      })
      image.setClipPath(clip);
    }
    this.#zr.add(image);
  }

  /**
   * @param {{
  *   image: String | HTMLImageElement | HTMLCanvasElement,
  *   text: String,
  *   x: Number,
  *   y: Number,
  *   width: Number,
  *   height: Number,
  *   r: Number | Number[],
  *   fill: String,
  *   fontWeight: String,
  *   fontSize: Number,
  *   fontFamily: String,
  *   textLineHeight: Number,
  *   textBackgroundColor: String,
  *   textPadding: Number | Number[],
  *   textAlign: String,
  *   textVerticalAlign: String,
  *   rich: Object,
  *   colorDark: String,
  *   colorLight: String,
  *   correctLevel: String
  * }} opts 
  */
  #addText(opts) {
    // 去除额外参数image/colorDark/colorLight/correctLevel
    const { image, colorDark, colorLight, correctLevel, ...style } = opts;
    const text = new zrender.Text({
      style,
    })
    this.#zr.add(text);
  }

  async #generateFromDataSource() {
    const dataSource = this.#opts.dataSource;
    dataSource.forEach(({ type, ...opts }) => {
      switch(type) {
        case "rect":
          this.#addRect(opts);
          return;
        case "image":
        case "qrcode":
          this.#addImage(opts);
          return;
        case "text":
        case "richtext":
          this.#addText(opts);
          return;
      }
    })
    await observeDOM(this.#dom);
    const canvas = this.#dom.childNodes[0].childNodes[0];
    const img = canvas.toDataURL(this.#opts.imageType, this.#opts.imageQuality);
    return img;
  }

  /**
   * @description 重设数据源dataSource
   * @param {{
  *   type: "rect" | "image" | "qrcode" | "text" | "richtext",
  *   image: String | HTMLImageElement | HTMLCanvasElement,
  *   text: String | dataSource[],
  *   x: Number,
  *   y: Number,
  *   width: Number,
  *   height: Number,
  *   r: Number | Number[],
  *   fill: String,
  *   fontWeight: String,
  *   fontSize: Number,
  *   fontFamily: String,
  *   textLineHeight: Number,
  *   textBackgroundColor: String,
  *   textPadding: Number | Number[],
  *   textAlign: String,
  *   textVerticalAlign: String,
  *   colorDark: String,
  *   colorLight: String,
  *   correctLevel: String
  * }[]} dataSource 
  */
  setDataSource(dataSource) {
    if (!isDataSourceValid(dataSource)) {
      console.log("数据为空或类型错误");
      return;
    }
    this.#loaded = false;
    this.#opts.dataSource = dataSource;
    this.#processDataSource();
  }

  /**
   * @param {Function} cb 回调函数,图片将会以参数形式传回
   */
  generate(cb) {
    if (!isDataSourceValid(this.#opts.dataSource)) {
      console.log("数据为空或类型错误");
      return;
    }
    if (typeOf(cb) !== "function") {
      console.log("回调非函数类型");
      return;
    }
    if (!this.#loaded) {
      setTimeout(() => this.generate(cb), 300);
      return;
    }
    this.#generateFromDataSource().then((img) => cb && cb(img));
  }
}

export default Generator;