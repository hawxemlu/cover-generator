/**
 * @description 检查dataSource是否为有效值(数组且不为空)
 */
function isDataSourceValid(dataSource) {
  if (!Array.isArray(dataSource)) {
    return false;
  }
  if (dataSource.length === 0) {
    return false;
  }
  return true;
}

/**
 * @description 监听DOM元素是否改变(触发绘制)
 * @param {HTMLElement}
 */
function observeDOM(dom) {
  return new Promise((resolve) => {
    const observer = new MutationObserver(resolve);
    observer.observe(dom, {
      childList: true,
      subtree: true
    })
  })
}

/**
 * @description 图片预加载
 * @param {String} url 
 */
function preloadImage(url) {
  return new Promise((resolve) => {
    const img = new Image();
    img.setAttribute("crossOrigin", "anonymous");
    img.src = url;
    img.onload = function() {
      resolve(img);
    }
  })
}

/**
 * @description 数据类型获取
 */
function typeOf(obj) {
  return Object.prototype.toString.call(obj).slice(8, -1).toLowerCase();
}

export { isDataSourceValid, observeDOM, preloadImage, typeOf }