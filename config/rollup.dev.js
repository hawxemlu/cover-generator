const serve = require("rollup-plugin-serve");
const livereload = require("rollup-plugin-livereload");
const commonConfig = require("./rollup.common.js");

const devPlugins = [
  serve({ 
    open: true, // 自动拉起浏览器打开新页面
    port: 8000,
    contentBase: ".",
    openPage: "/public/index.html",
  }),
  livereload({
    watch: [
      "build",
      "public"
    ]
  })
]

const devConfig = { ...commonConfig };
devConfig.output.forEach((item) => item.file = item.file.replace(".min", ""));
devConfig.plugins.push(...devPlugins);

module.exports = devConfig;
